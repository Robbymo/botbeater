<?php

   $REFRESH_SECS = 60;
   $NUM_CHARS = 500;
   $prev = isset($_GET['prev']) ? $_GET['prev'] : 'initial-toggle';
   $prev_array = explode(',',$prev);
   $prev = str_replace(' ', '%20', $prev);

   // Set items to check including url, button text to search for and title to display
   $check = array(
      array(
         'title' => 'Playstation 5 (Disc)',
         'url' => 'https://www.bestbuy.com/site/sony-playstation-5-console/6426149.p?skuId=6426149',
         'text' => 'Sold Out',
         'chars' => 5500,
      ),
      array(
         'title' => 'Playstation 5 (Digital)',
         'url' => 'https://www.bestbuy.com/site/sony-playstation-5-digital-edition-console/6430161.p?skuId=6430161',
         'text' => 'Sold Out',
         'chars' => 5500,
      ),
      // array(
      //    'title' => 'Playstation 5 (Digital)',
      //    'url' => 'https://www.gamestop.com/video-games/playstation-5/consoles/products/playstation-5-digital-edition/11108141.html',
      //    'text' => 'Not Available',
      //    'chars' => 1000,
      // ),
      // array(
      //    'title' => 'Sony - PlayStation 5 - DualSense Charging Station',
      //    'url' => 'https://www.bestbuy.com/site/sony-playstation-5-dualsense-charging-station-white/6430165.p?skuId=6430165',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'Playstation 5 (Disc)',
      //    'url' => 'https://www.newegg.com/p/N82E16868110293',
      //    'text' => 'Out of Stock',
      // ),
      array(
         'title' => 'RTX 3060 TI',
         'url' => 'https://www.bestbuy.com/site/nvidia-geforce-rtx-3060-ti-8gb-gddr6-pci-express-4-0-graphics-card-steel-and-black/6439402.p?skuId=6439402',
         'text' => 'Sold Out',
      ),
      // array(
      //    'title' => 'Playstation 5 (Disc)',
      //    'url' => 'https://www.target.com/p/playstation-5-console/-/A-81114595',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'ROG Strix 3090',
      //    'url' => 'https://www.bestbuy.com/site/asus-geforce-rtx-3090-24gb-gddr6x-pci-express-4-0-strix-graphics-card-black/6432447.p?skuId=6432447',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'Ryzen 9 5950x',
      //    'url' => 'https://www.bestbuy.com/site/amd-ryzen-9-5950x-4th-gen-16-core-32-threads-unlocked-desktop-processor-without-cooler/6438941.p?skuId=6438941',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'MSI RX 6800 XT',
      //    'url' => 'https://www.bestbuy.com/site/msi-radeon-rx-6800-xt-16g-16gb-gddr6-pci-express-4-0-graphics-card-black-black/6440913.p?skuId=6440913',
      //    'text' => 'Sold Out',
      //    'chars' => 1000,
      // ),
      // array(
      //    'title' => 'XFX RX 6800 XT',
      //    'url' => 'https://www.bestbuy.com/site/xfx-amd-radeon-rx-6800xt-16gb-gddr6-pci-express-4-0-gaming-graphics-card-black/6441226.p?skuId=6441226',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'RTX 3080',
      //    'url' => 'https://www.bestbuy.com/site/nvidia-geforce-rtx-3080-10gb-gddr6x-pci-express-4-0-graphics-card-titanium-and-black/6429440.p?skuId=6429440',
      //    'text' => 'Sold Out',
      // ),
      array(
         'title' => 'RTX 3070',
         'url' => 'https://www.bestbuy.com/site/nvidia-geforce-rtx-3070-8gb-gddr6-pci-express-4-0-graphics-card-dark-platinum-and-black/6429442.p?skuId=6429442',
         'text' => 'Sold Out',
      ),
      // array(
      //    'title' => 'RX 5700 XT',
      //    'url' => 'https://www.bestbuy.com/site/msi-mech-oc-amd-radeon-rx-5700-xt-8gb-gddr6-pci-express-4-0-graphics-card-black/6374966.p?skuId=6374966',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'RX 590',
      //    'url' => 'https://www.bestbuy.com/site/xfx-amd-radeon-rx-570-rs-black-edition-8gb-gddr5-pci-express-3-0-graphics-card-black-red/6202343.p?skuId=6202343',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'RX 580',
      //    'url' => 'https://www.bestbuy.com/site/xfx-amd-radeon-rx-580-gts-black-edition-8gb-gddr5-pci-express-3-0-graphics-card-black/6092641.p?skuId=6092641',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'RX 570',
      //    'url' => 'https://www.bestbuy.com/site/xfx-amd-radeon-rx-570-rs-black-edition-8gb-gddr5-pci-express-3-0-graphics-card-black-red/6202343.p?skuId=6202343',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'GTX 1060',
      //    'url' => 'https://www.bestbuy.com/site/pny-nvidia-geforce-gtx-1060-6gb-gddr5-pci-express-3-0-graphics-card-black/5507806.p?skuId=5507806',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'EVGA 1000w PSU',
      //    'url' => 'https://www.bestbuy.com/site/evga-gp-series-supernova-1000w-atx-80-plus-gold-fully-modular-power-supply-black/6209800.p?skuId=6209800',
      //    'text' => 'Check Stores',
      // ),
      // array(
      //    'title' => 'Corsair 1000w PSU',
      //    'url' => 'https://www.bestbuy.com/site/corsair-rmx-series-1000w-atx12v-2-4-eps12v-2-92-80-plus-gold-modular-power-supply-black/6073500.p?skuId=6073500',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'Gigabyte 3060ti',
      //    'url' => 'https://www.bestbuy.com/site/gigabyte-nvidia-geforce-rtx-3060-ti-gaming-oc-8g-gddr6-pci-express-4-0-graphics-card-black/6442484.p?skuId=6442484',
      //    'text' => 'Sold Out',
      //    'chars' => 1000,
      // ),
      // array(
      //    'title' => 'Gigabyte 3060ti Eagle',
      //    'url' => 'https://www.bestbuy.com/site/gigabyte-nvidia-geforce-rtx-3060-ti-eagle-oc-8g-gddr6-pci-express-4-0-graphics-card-black/6442485.p?skuId=6442485',
      //    'text' => 'Sold Out',
      // ),
      // array(
      //    'title' => 'EVGA 3060ti FTW3',
      //    'url' => 'https://www.bestbuy.com/site/evga-nvidia-geforce-rtx-3060-ti-ftw3-gaming-8gb-gddr6-pci-express-4-0-graphics-card/6444444.p?skuId=6444444',
      //    'text' => 'Sold Out',
      //    'chars' => 5500,
      // ),
      // array(
      //    'title' => 'EVGA 1650 Super',
      //    'url' => 'https://www.bestbuy.com/site/evga-super-sc-ultra-gaming-nvidia-geforce-gtx-1650-super-4gb-gddr6-pci-express-3-0-graphics-card-black-silver/6412939.p?skuId=6412939',
      //    'text' => 'Sold Out',
      // ),
      array(
         'title' => 'ASUS 1660 Super',
         'url' => 'https://www.bestbuy.com/site/asus-nvidia-geforce-gtx-1660-super-oc-edition-6gb-gddr6-pci-express-3-0-graphics-card-black-gray/6405063.p?skuId=6405063',
         'text' => 'Sold Out',
      ),
      array(
         'title' => 'Gigabyte GTX 1660 Super',
         'url' => 'https://www.bestbuy.com/site/gigabyte-nvidia-geforce-gtx-1660-super-oc-edition-6gb-gddr6-pci-express-3-0-graphics-card-black/6409171.p?skuId=6409171',
         'text' => 'Sold Out',
      ),
      // array(
      //    'title' => 'Gigabyte GTX 1660 Super OC',
      //    'url' => 'https://www.bestbuy.com/site/pny-xlr8-gaming-single-fan-nvidia-geforce-gtx-1660-super-overclocked-edition-6gb-gddr6-pci-express-3-0-graphics-card-black/6407309.p?skuId=6407309',
      //    'text' => 'Sold Out',
      //    'chars' => 5000,
      // ),
      // array(
      //    'title' => 'Philips Hue Gradient',
      //    'url' => 'https://www.bestbuy.com/site/philips-hue-play-gradient-lightstrip-55/6427737.p?skuId=6427737',
      //    'text' => 'Unavailable Nearby',
      // ),
      array(
         'title' => 'BuildAPC',
         'url' => 'https://old.reddit.com/r/buildapcsales/new/',
         //'text' => (isset($prev_array[0]) ? $prev_array[0] : ''),
         'text' => substr($prev, 0, 100),
         'chars' => 2500,
         'audio' => 'ship_bell.mp3',
         'cache' => 0,
         'persist' => 1,
      ),
      // array(
      //    'title' => 'HardwareSwap',
      //    'url' => 'https://old.reddit.com/r/hardwareswap/new/',
      //    'text' => (isset($prev_array[1]) ? $prev_array[1] : ''),
      //    'chars' => 2500,
      //    'audio' => 'shooting_star.mp3',
      //    'cache' => 0,
      //    'persist' => 1,
      // ),
      // array(
      //    'title' => 'B&H',
      //    'url' => 'https://www.bhphotovideo.com/c/products/Graphic-Cards/ci/6567/N/3668461602?filters=fct_a_filter_by%3A03_INSTOCK',
      //    'text' => (isset($prev_array[1]) ? $prev_array[1] : ''),
      //    'chars' => 2500,
      //    'audio' => 'shooting_star.mp3',
      //    'cache' => 0,
      // ),
      // array(
      //    'title' => 'BB Vid Cards',
      //    'url' => 'https://www.bestbuy.com/site/computer-cards-components/video-graphics-cards/abcat0507002.c?id=abcat0507002&qp=soldout_facet%3DAvailability~Exclude%20Out%20of%20Stock%20Items',
      //    'text' => (isset($prev_array[2]) ? $prev_array[2] : ''),
      //    'chars' => 2500,
      //    'audio' => 'shooting_star.mp3',
      //    'cache' => 0,
      //    'persist' => 1,
      // ),
   );

   // Define retailer url, unique element to find on product page and retailer logo (logo can be local or external)
   $retailers = array(
      array(
         'url' => 'bestbuy.com',
         'selector' => 'fulfillment-add-to-cart-button',
         'logo' => 'images/bestbuy.png',
      ),
      array(
         'url' => 'playstation.com',
         'selector' => '<productHero-info>',
         'logo' => '',
      ),
      array(
         'url' => 'newegg.com',
         'selector' => 'product-inventory',
         'logo' => 'images/newegg.png',
      ),
      array(
         'url' => 'gamestop.com',
         'selector' => 'add-to-cart-buttons',
         'logo' => 'images/gamestop.svg',
      ),
      array(
         'url' => 'target.com',
         'selector' => 'flexible-fulfillment',
         'logo' => 'images/target.png',
      ),
      array(
         'url' => 'reddit.com',
         'selector' => '<span class="rank">1</span>',
         'logo' => 'images/reddit.png',
         'persist' => 1,
      ),
      array(
         'url' => 'bhphotovideo.com',
         'selector' => '<span data-selenium="titleNumberingPagination"',
         'logo' => 'images/reddit.png',
         'persist' => 1,
      ),
   );

$num_success = 0;
$willreload = false;
$audio_on = false;
$new_prev = '';
// Process each item
foreach ($check as $item) {
   $selector = '';
   $logo = '';
   $status = '';
   $audio = isset($item['audio']) ? $item['audio'] : (!empty($audio) ? $audio : '');
   // Open curl connection to url
   $curl_handle=curl_init();
   $url_nocache = ((isset($item['cache']) && $item['cache'] == 0) ? $item['url'] : addCacheBuster($item['url']));
	curl_setopt($curl_handle, CURLOPT_URL, $url_nocache);
   //curl_setopt($curl_handle, CURLOPT_VERBOSE, true);
   //curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (K HTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36');
	$query = curl_exec($curl_handle);
   if(!$query) {
      echo 'curl error';
      echo curl_error($curl_handle);
   }
	curl_close($curl_handle);

   // Get selector to find based on retailer url
   foreach($retailers as $retailer) {
      if(strpos($item['url'], $retailer['url']) !== false) {
         $item['retailer'] = $retailer['url'];
         if($retailer['url'] == 'bestbuy.com' && isset($item['persist'])){

         }
         $selector = $retailer['selector'];
         //$persist = isset($retailer['persist']) ? true : false;
         $logo = !empty($retailer['logo']) ? '<div class="retailer-logo"><img src="'.$retailer['logo'].'"></div>': '';
         continue;
      }
   }
   if(empty($selector)){
      continue;
   }
   $num_success++;

   $persist = isset($item['persist']) ? true : false;

   // Get position of selector searched for
   $button_pos = stripos($query, $selector);
   echo '<span class="position">Button Position: '.$button_pos.'</span><br>';

   // Get next X characters after matched selector
   $button = substr($query, $button_pos, (isset($item['chars']) ? $item['chars'] : $NUM_CHARS));

   // Check to see if searched text i.e. "Sold Out" is found or not
   if(stripos($button, $item['text']) === false && !empty($button_pos)){
      // Searched text is not found which means our button / element text has changed (product can be purchased)
      echo '<div class="item-wrap bg-green">'.$logo.'<div class="item-status"><img src="images/green-check.png"></div><div class="item-header">'.($persist ? $item['text'] : 'BUY!!!').'</div><a href="'.$url_nocache.'" target="_blank">'.$item['title'].'<img src="images/ext-link.svg"></a>';
      // If Best Buy add API add to cart url
      if(preg_match('/bestbuy\.com.*\/([0-9]+)\.p\?/', $item['url'], $match)){
         echo '<div class="add-to-cart"><a href="https://api.bestbuy.com/click/-/'.$match[1].'/cart" target="_blank">CLICK HERE TO ADD TO CART</a></div>';
      }
      //$prev = '';
      if(!$audio_on && !isset($item['mute'])){
         ?>
         <audio class="alert" controls autoplay volume="0.5" src="<?php echo !empty($audio) ? $audio : 'railroad_alert.mp3';?>">Your browser does not support the <code>audio</code> element.</audio>
<!--          <script type="text/javascript">
            alert('FOUND!');
         </script> -->
         <?php
            $audio_on = true;
      }
      echo '</div>';
      error_log('Found one: '.$item['url'].' at '.date("M,d,Y h:i:s A"));
   }else{
      // Searched text is found so our button / element text has not changed
      echo '<div class="item-wrap bg-red">'.$logo.'<div class="item-status"><img src="images/red-x.png"></div><div class="item-header">'.$item['text'].'</div><a href="'.$url_nocache.'" target="_blank">'.$item['title'].'<img src="images/ext-link.svg"></a></div>';
   }
   if(!$willreload || $persist){
      if($persist && $item['retailer'] == 'reddit.com') {
         preg_match('/href=\"(.+?)\"\s/', $button, $match);
         if(isset($match[1])){
            //$prev = ($prev == 'initial-toggle') ? $match[1] : (stripos($prev, $match[1]) === false ? $prev.','.$match[1] : $prev);
            $new_prev = ($prev == 'initial-toggle' && empty($new_prev)) ? $match[1] : (empty($new_prev) ? $match[1] : $new_prev.','.$match[1]);
         }
      }
      if($persist && $item['retailer'] == 'bhphotovideo.com') {
         preg_match('/>(.*?)<\/span/', $button, $match2);
         if(isset($match2[1])){
            //$prev = ($prev == 'initial-toggle') ? $match[1] : (stripos($prev, $match[1]) === false ? $prev.','.$match[1] : $prev);
            $new_prev = ($prev == 'initial-toggle' && empty($new_prev)) ? $match2[1] : (empty($new_prev) ? $match2[1] : $new_prev.','.$match2[1]);
         }
      }
      if($persist && $item['retailer'] == 'bestbuy.com') {
         preg_match('/>(.*?)<\/span/', $button, $match3);
         if(isset($match3[1])){
            //$prev = ($prev == 'initial-toggle') ? $match[1] : (stripos($prev, $match[1]) === false ? $prev.','.$match[1] : $prev);
            $new_prev = ($prev == 'initial-toggle' && empty($new_prev)) ? $match3[1] : (empty($new_prev) ? $match3[1] : $new_prev.','.$match3[1]);
         }
      }
      $refresh = '<meta http-equiv="refresh" content="'.$REFRESH_SECS.'; url=botbeater.php'.(!empty($new_prev) ? '?prev='.$new_prev : '').'">';
      $persist = false;
      $willreload = true;
   }
}
if (isset($refresh) && !empty($refresh)) {
   echo $refresh;
}
$num_success = $num_success > 0 ? $num_success : 1;
?>

<style type="text/css">
   html, body {
      background: #2f2424;
   }
   span.notice {
      font-weight: bold;
      color: #38fb6b;
   }
   span.position {
      color: #fff;
      font-size: 10px;
   }
   div.item-wrap {
      width: calc(100% - 10px);
      /*height: <?php echo intval(580 / $num_success).'px';?>;*/
      height: <?php echo intval(620 / $num_success).'px';?>;
      font-size: <?php echo (36 - ($num_success*2)).'px';?>;
      text-align: center;
      margin-bottom: <?php echo (25 - intval($num_success * 2.5)).'px';?>;
      position: relative;
      border-radius: 10px;
      background: #f1f3f4;
   }
   div.retailer-logo,
   div.item-status {
      display: flex;
      /*justify-content: center;*/
      justify-content: left;
      align-items: center;
      position: absolute;
      left: 20px;
      height: 100%;
      min-width: 150px;
   }
   div.item-status {
    right: 20px;
    left: auto;
    min-width: auto;
   }
   div.item-status img {
      max-height: 60%;
   }
   div.retailer-logo img {
      max-height: <?php echo intval((580 / $num_success)*.6).'px';?>;
   }
   div.bg-green {
      border: 5px solid #abff97;
   }
   div.bg-red {
      border: 5px solid #f75656;
   }
   div.item-wrap a {
      margin-top: 12px;
      font-size: <?php echo (26 - intval($num_success * .67)).'px';?>;
      display: block;
      font-weight: bold;
      color: #000;
      font-family: Arial, Helvetica, sans-serif;
      text-decoration: none;
   }
   div.item-wrap a img {
      width: 17px;
      vertical-align: top;
      margin-left: 8px;
      padding-top: 2px;
   }
   div.item-header {
      font-weight: bold;
      padding-top: 5px;
      font-family: "Arial Black", Gadget, sans-serif;
   }
   div.add-to-cart {
      position: absolute;
      top: -7px;
      right: 142px;
   }
   div.add-to-cart a {
      font-size: <?php echo (22 - $num_success).'px';?>;
      text-decoration: none;
      color: #000;
      font-weight: bold;
      font-family: Impact, Charcoal, sans-serif;
   }
   audio.alert {
      position: absolute;
      bottom: 5px;
      right: 75px;
      background: #f1f3f4;
      max-height: 25px;
      max-width: 250px;
   }
</style>

<?php

// Takes url and adds random url parameter in the form of ?abc=123456789
function addCacheBuster($url, $key_length = 3) {
   $query = parse_url($url, PHP_URL_QUERY);

   // Check if url paramater already exists before adding new one
   if ($query) {
       $url .= '&';
   } else {
       $url .= '?';
   }
   return $url.substr(str_shuffle(str_repeat($x='abcdefghijklmnopqrstuvwxyz', ceil($key_length/strlen($x)) )),1,$key_length).'='.rand();
}
